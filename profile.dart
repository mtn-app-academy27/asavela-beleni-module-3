import 'package:flutter/material.dart';
import 'package:module_3/login_page.dart';

class ProfilePage extends StatefulWidget {
  const ProfilePage({Key? key}) : super(key: key);

  @override
  State<ProfilePage> createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> {
  TextEditingController emailController = TextEditingController();
  TextEditingController passwordController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Profile'),
        automaticallyImplyLeading: false,
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          const CircleAvatar(
            radius: 80,
            backgroundImage: AssetImage('assets/profile.jpg'),
          ),
          const SizedBox(height: 10),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: TextField(
              controller: emailController,
              keyboardType: TextInputType.text,
              decoration: const InputDecoration(
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.all(Radius.circular(20)),
                ),
                labelText: 'Please enter your email address',
                labelStyle: TextStyle(
                  color: Colors.blue,
                ),
              ),
            ),
          ),
          const SizedBox(
            height: 10,
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: TextField(
              controller: passwordController,
              obscureText: true,
              keyboardType: TextInputType.text,
              decoration: const InputDecoration(
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.all(Radius.circular(20)),
                ),
                labelText: 'Please enter your password',
                labelStyle: TextStyle(
                  color: Colors.blue,
                ),
              ),
            ),
          ),
          const SizedBox(
            height: 10,
          ),
          ElevatedButton(
              onPressed: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => const DashBoardPage(),
                    ));
              },
              child: const Text('dashbord'))
        ],
      ),
    );
  }
}

class MarksPage extends StatefulWidget {
  const MarksPage({Key? key}) : super(key: key);

  @override
  State<MarksPage> createState() => _MarksPageState();
}

class _MarksPageState extends State<MarksPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Marks'),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            const Text(
              'Marks page',
              style: TextStyle(
                  color: Colors.blue,
                  fontSize: 40,
                  fontWeight: FontWeight.bold),
            ),
            const SizedBox(
              height: 25,
            ),
            const Text(
              'You got 100/100 for module 1',
              style: TextStyle(
                color: Colors.blue,
                fontSize: 18,
              ),
            ),
            const SizedBox(
              height: 30,
            ),
            const Text(
              'You got 100/100 for module 2',
              style: TextStyle(
                color: Colors.blue,
                fontSize: 18,
              ),
            ),
            const SizedBox(
              height: 30,
            ),
            const Text(
              'You got 98/100 for module 3',
              style: TextStyle(
                color: Colors.blue,
                fontSize: 18,
              ),
            ),
            const SizedBox(
              height: 30,
            ),
            const Text(
              'You got 80/100 for module 4',
              style: TextStyle(
                color: Colors.blue,
                fontSize: 18,
              ),
            ),
            const SizedBox(
              height: 30,
            ),
            const Text(
              'You got 100/100 for module 5',
              style: TextStyle(
                color: Colors.blue,
                fontSize: 18,
              ),
            ),
            const SizedBox(
              height: 20,
            ),
            ElevatedButton(
              onPressed: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => const AssessmentsPage(),
                    ));
              },
              child: const Text(
                'view assessments',
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class AssessmentsPage extends StatefulWidget {
  const AssessmentsPage({Key? key}) : super(key: key);

  @override
  State<AssessmentsPage> createState() => _AssessmentsPageState();
}

class _AssessmentsPageState extends State<AssessmentsPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text('Assessments')),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            const Text(
              'Assessments page',
              style: TextStyle(
                  color: Colors.blue,
                  fontSize: 40,
                  fontWeight: FontWeight.bold),
            ),
            const SizedBox(
              height: 20,
            ),
            const Text(
              'module 1',
              style: TextStyle(
                color: Colors.blue,
                fontSize: 14,
              ),
            ),
            const SizedBox(
              height: 10,
            ),
            const Text(
              'module 2',
              style: TextStyle(
                color: Colors.blue,
                fontSize: 14,
              ),
            ),
            const SizedBox(
              height: 10,
            ),
            const Text(
              'module 3',
              style: TextStyle(
                color: Colors.blue,
                fontSize: 14,
              ),
            ),
            const SizedBox(
              height: 10,
            ),
            const Text(
              'module 4',
              style: TextStyle(
                color: Colors.blue,
                fontSize: 14,
              ),
            ),
            const Text(
              'module 5',
              style: TextStyle(
                color: Colors.blue,
                fontSize: 14,
              ),
            ),
            const SizedBox(
              height: 10,
            ),
            ElevatedButton(
              onPressed: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => const MarksPage(),
                    ));
              },
              child: const Text(
                'marks',
              ),
            ),
          ],
        ),
      ),
    );
  }
}
